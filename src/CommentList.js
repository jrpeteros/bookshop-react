//CommentList.js
import React, { Component } from 'react';
import style from './style';
class CommentList extends Component {

render() {
	const {data} = this.props;

	let bookList = data.map(data => {
		 return (
		 	<div className="card"  key={ data['_id'] }>
			  <div className="card-block">
			    <h3 className="card-title"> {data.name}</h3>
			    <h4 className="card-title"> {data.series ? "series: "+data.series : ""}</h4>
			    <p className="card-text" hidden={!data.name} style={style.seriesText}> by: {data.author}</p>
			  </div>
			</div>

			
		 )
	})

	 return (
	 	<div style={ style.commentList }>
	 		{bookList}
	 	</div>
	 )
 }
}
export default CommentList;
