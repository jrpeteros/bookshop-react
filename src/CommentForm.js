//CommentForm.js
import React, { Component } from 'react';
import style from './style';

class CommentForm extends Component {
	constructor(props) {
	 super(props);
		 this.state = { author: ''};
		 this.handleAuthorChange = this.handleAuthorChange.bind(this);
		 this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleAuthorChange(e) {
		var currentTarget = e.target;
	 	this.setState({author: currentTarget.value}, 
		    function () {
		       this.handleSubmit(currentTarget);
		    }
		)
	}

	 handleSubmit(e) {
		//e.preventDefault();
		let author = this.state.author.trim();
		 if (!author) {
		 	this.props.loadCommentsFromServer();
		 }
		 this.props.onCommentSubmit({ author: author });
	}

 	render() {
	 return (
		 <div>
		 	<div className="input-group">
			  	<input type="text" className="form-control" 
			  		placeholder="Search for Author or Book title"
				 	value={ this.state.author || "" }
				 	onChange={ this.handleAuthorChange } 
			  	/>
			   <span className="input-group-addon glyphicon-search" style={style.glyphicons}></span>    
			</div>
		</div>
		 
	 )
 }
}
export default CommentForm;