//CommentBox.js
import React, { Component } from 'react';
import CommentList from './CommentList';
import CommentForm from './CommentForm';
import axios from 'axios';
import style from './style';
var findApi = 'http://localhost:3031/api/author';

class CommentBox extends Component {

	constructor(props) {
	 	super(props);
	 	this.loadCommentsFromServer = this.loadCommentsFromServer.bind(this);
	 	this.handleFindAuthor = this.handleFindAuthor.bind(this);
	 	this.state = { data: [] };

	 }

	loadCommentsFromServer() {
	 	axios.get(this.props.url).then(res => {
	 		this.setState({ data: res.data });
	 	})
	}

	handleFindAuthor(book) {
		axios.post(findApi, book, {
	 		headers:{
	 			'Content-Type': 'application/json',
	 		}
	 	}).then(res => {
	 		this.setState({ data: res.data });
	 	}).catch(function (error){
	 		console.log(error);
	 	});
	}

	componentWillMount() {
		 this.loadCommentsFromServer();
	 }

	render() {
		return (
			 <div style={ style.commentBox }>
			 	<h2>Looking for something?</h2>
			 	<CommentForm onCommentSubmit={ this.handleFindAuthor } loadCommentsFromServer ={ this.loadCommentsFromServer} />
			 	<CommentList data={ this.state.data }/>
			 </div>
		)
	}

}
export default CommentBox;